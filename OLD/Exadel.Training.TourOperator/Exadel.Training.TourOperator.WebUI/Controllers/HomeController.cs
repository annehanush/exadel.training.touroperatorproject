﻿using Exadel.Training.TourOperator.Data.Abstract;
using Exadel.Training.TourOperator.Data.Entities;
using Exadel.Training.TourOperator.WebUI.Areas.AccountArea.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Exadel.Training.TourOperator.WebUI.Controllers
{
    public class HomeController : Controller
    {
        TourOperatorDbContext db = new TourOperatorDbContext();
        ApplicationContext context = new ApplicationContext();

        public ApplicationUser CurrentUser { get; private set; }

        private ICountryRepository countryRepository;


        public HomeController(ICountryRepository countryRepository)
        {
            this.countryRepository = countryRepository;
        }

        /// <summary>
        /// Check info about an existence of current user.
        /// </summary>
        /// <returns></returns>
        public string CheckUser()
        {
            string user = "";

            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            CurrentUser = UserManager.FindById(User.Identity.GetUserId());

            try
            {
                user = CurrentUser.UserName;
            }
            catch (NullReferenceException)
            {
                user = "";
            }

            return user;
        }

        /// <summary>
        /// Index view.
        /// </summary>
        /// <returns></returns>
        public ViewResult Index()
        {
            ViewBag.User = CheckUser();

            return View();
        }

        /// <summary>
        /// Countries view. Gets info about all countries.
        /// </summary>
        /// <returns></returns>
        public ViewResult Countries()
        {
            ViewBag.User = CheckUser();

            return View(countryRepository.Countries);
        }

        /// <summary>
        /// Info about choosed country. Returns partial view.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CertainCountryInfo(int id)
        {
            Country country = db.Countries.Find(id);

            return PartialView(country);
        }

    }
}