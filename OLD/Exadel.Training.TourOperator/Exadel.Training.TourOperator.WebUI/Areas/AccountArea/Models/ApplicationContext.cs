﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Exadel.Training.TourOperator.WebUI.Areas.AccountArea.Models
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base("name=TourOperatorDbContext") { }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }
    }
}