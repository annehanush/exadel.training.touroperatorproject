namespace Exadel.Training.TourOperator.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SetOfTYD")]
    public partial class SetOfTYD
    {
        public int Id { get; set; }

        public int TypeOfDietId { get; set; }

        public int HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }

        public virtual TypeOfDiet TypeOfDiet { get; set; }
    }
}
