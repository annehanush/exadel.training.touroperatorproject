namespace Exadel.Training.TourOperator.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SetOfRoom
    {
        public int Id { get; set; }

        public int HotelId { get; set; }

        public int RoomId { get; set; }

        public int? AmountOfRooms { get; set; }

        [StringLength(50)]
        public string Square { get; set; }

        public int? PeopleAmount { get; set; }

        public string Info { get; set; }

        public virtual Hotel Hotel { get; set; }

        public virtual Room Room { get; set; }
    }
}
