﻿CREATE TABLE [dbo].[SetOfRooms]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [HotelId] INT NOT NULL, 
    [RoomId] INT NOT NULL, 
    [AmountOfRooms] INT NULL, 
    [Square] NVARCHAR(50) NULL, 
    [PeopleAmount] INT NULL, 
    [Info] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_SetOfRooms_Hotel] FOREIGN KEY ([HotelId]) REFERENCES [dbo].[Hotel]([Id]), 
    CONSTRAINT [FK_SetOfRooms_Room] FOREIGN KEY ([RoomId]) REFERENCES [dbo].[Room]([Id])
)
