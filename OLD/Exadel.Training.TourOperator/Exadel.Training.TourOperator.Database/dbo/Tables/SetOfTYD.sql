﻿CREATE TABLE [dbo].[SetOfTYD]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [TypeOfDietId] INT NOT NULL, 
    [HotelId] INT NOT NULL, 
    CONSTRAINT [FK_SetOfTYD_TypeOfDiet] FOREIGN KEY ([TypeOfDietId]) REFERENCES [dbo].[TypeOfDiet]([Id]), 
    CONSTRAINT [FK_SetOfTYD_Hotel] FOREIGN KEY ([HotelId]) REFERENCES [dbo].[Hotel]([Id])
)
