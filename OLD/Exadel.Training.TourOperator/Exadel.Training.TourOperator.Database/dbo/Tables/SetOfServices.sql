﻿CREATE TABLE [dbo].[SetOfServices]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [HotelId] INT NOT NULL, 
    [ServiceId] INT NOT NULL, 
    [IsFree] BIT NOT NULL, 
    CONSTRAINT [FK_SetOfServices_Hotel] FOREIGN KEY ([HotelId]) REFERENCES [dbo].[Hotel]([Id]), 
    CONSTRAINT [FK_SetOfServices_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service]([Id])
)
