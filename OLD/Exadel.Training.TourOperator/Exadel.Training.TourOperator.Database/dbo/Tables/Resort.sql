﻿CREATE TABLE [dbo].[Resort]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Airport] NVARCHAR(50) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [CountryId] INT NOT NULL, 
    [NameRUS] NVARCHAR(50) NULL, 
    CONSTRAINT [FK_Resort_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country]([Id])
)
