﻿CREATE TABLE [dbo].[Tour]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Cost] MONEY NOT NULL, 
    [NumberOfPersons] INT NOT NULL, 
    [NumberOfNights] INT NOT NULL, 
    [HotelId] INT NOT NULL, 
    [TransportId] INT NOT NULL, 
    CONSTRAINT [FK_Tour_Hotel] FOREIGN KEY ([HotelId]) REFERENCES [dbo].[Hotel]([Id]), 
    CONSTRAINT [FK_Tour_Transport] FOREIGN KEY ([TransportId]) REFERENCES [dbo].[Transport]([Id])
)
