﻿CREATE TABLE [dbo].[Request]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Insurance] BIT NOT NULL, 
    [DateOfCreation] DATE NOT NULL, 
    [TourId] INT NOT NULL, 
    [ClientId] INT NOT NULL, 
    CONSTRAINT [FK_Request_Tour] FOREIGN KEY ([TourId]) REFERENCES [dbo].[Tour]([Id]), 
    CONSTRAINT [FK_Request_Client] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Client]([Id])
)
