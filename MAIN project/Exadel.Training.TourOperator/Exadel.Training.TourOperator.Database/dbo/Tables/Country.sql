﻿CREATE TABLE [dbo].[Country]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [VisaCost] MONEY NULL, 
    [Currency] NVARCHAR(50) NOT NULL, 
    [Language] NVARCHAR(50) NOT NULL, 
    [Info] NVARCHAR(MAX) NULL, 
    [NameRUS] NVARCHAR(50) NULL
)
