﻿CREATE TABLE [dbo].[Client]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Phone] NVARCHAR(50) NULL, 
    [Email] NVARCHAR(50) NOT NULL, 
    [DateOfBirth] DATE NULL, 
    [Gender] NCHAR(10) NULL, 
    [UserId] INT NOT NULL, 
    CONSTRAINT [FK_Client_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User]([Id])
)
