﻿CREATE TABLE [dbo].[Transport]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [TypeId] INT NOT NULL, 
    [ClassId] INT NOT NULL, 
    CONSTRAINT [FK_Transport_TransportType] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[TransportType]([Id]), 
    CONSTRAINT [FK_Transport_TransportClass] FOREIGN KEY ([ClassId]) REFERENCES [dbo].[TransportClass]([Id])
)
