﻿CREATE TABLE [dbo].[Tour]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Cost] MONEY NOT NULL, 
    [NumberOfPersons] INT NOT NULL, 
    [NumberOfNights] INT NOT NULL, 
    [HotelId] INT NOT NULL, 
    [TransportId] INT NOT NULL, 
    [HotelWithRoomId] INT NOT NULL, 
    [HotelWithTYDId] INT NOT NULL, 
    [DepartureCityId] INT NOT NULL, 
    [DepartureDate] DATE NOT NULL, 
    CONSTRAINT [FK_Tour_Hotel] FOREIGN KEY ([HotelId]) REFERENCES [dbo].[Hotel]([Id]), 
    CONSTRAINT [FK_Tour_Transport] FOREIGN KEY ([TransportId]) REFERENCES [dbo].[Transport]([Id]), 
    CONSTRAINT [FK_Tour_HotelWithRoom] FOREIGN KEY ([HotelWithRoomId]) REFERENCES [dbo].[HotelWithRoom]([Id]), 
    CONSTRAINT [FK_Tour_HotelWithTYD] FOREIGN KEY ([HotelWithTYDId]) REFERENCES [dbo].[HotelWithTYD]([Id]), 
    CONSTRAINT [FK_Tour_DepartureCity] FOREIGN KEY ([DepartureCityId]) REFERENCES [dbo].[DepartureCity]([Id])
)
