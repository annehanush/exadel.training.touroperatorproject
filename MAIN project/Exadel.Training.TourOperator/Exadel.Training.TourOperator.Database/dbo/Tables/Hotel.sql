﻿CREATE TABLE [dbo].[Hotel]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Rating] INT NOT NULL, 
    [ResortId] INT NOT NULL, 
    [Location] NVARCHAR(MAX) NOT NULL, 
    [Phone] NVARCHAR(50) NOT NULL, 
    [WebSite] NVARCHAR(50) NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_Hotel_Resort] FOREIGN KEY ([ResortId]) REFERENCES [dbo].[Resort]([Id])
)
