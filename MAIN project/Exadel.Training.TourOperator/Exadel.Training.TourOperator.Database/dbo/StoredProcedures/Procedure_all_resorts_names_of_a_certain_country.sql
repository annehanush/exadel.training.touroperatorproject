﻿CREATE PROCEDURE [dbo].[Procedure_all_resorts_of_a_certain_country]
	@countryName NVarChar(50)
AS
	SELECT Name FROM Resort WHERE CountryId IN ( SELECT Id FROM Country WHERE Name = @countryName )
