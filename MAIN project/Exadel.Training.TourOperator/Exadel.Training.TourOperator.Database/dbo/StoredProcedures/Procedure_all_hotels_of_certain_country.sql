﻿CREATE PROCEDURE [dbo].[Procedure_all_hotels_of_certain_country]
	@countryName NVARCHAR(50)
AS
	SELECT * FROM Hotel WHERE ResortId IN ( SELECT Id FROM Resort WHERE CountryId = ( SELECT Id FROM Country WHERE Name = @countryName ) )
