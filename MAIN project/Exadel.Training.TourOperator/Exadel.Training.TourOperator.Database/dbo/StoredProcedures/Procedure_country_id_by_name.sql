﻿CREATE PROCEDURE [dbo].[Procedure_country_id_by_name]
	@name NVarChar(50),
	@id int output
AS
	SELECT @id = Id FROM Country WHERE Name = @name
