﻿CREATE PROCEDURE [dbo].[Procedure_all_nights_amounts_for_certain_country]
	@countryName NVARCHAR(50)
AS
	SELECT DISTINCT NumberOfNights FROM Tour WHERE HotelId IN ( SELECT Id FROM Hotel WHERE ResortId IN ( SELECT Id FROM Resort WHERE CountryId = (SELECT Id FROM Country WHERE Name = @countryName) ) )
