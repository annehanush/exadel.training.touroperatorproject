﻿CREATE PROCEDURE [dbo].[Procedure_all_tours_names_for_certain_country_and_departure_city]
	@countryName NVarChar(50),
	@departureCity NVARCHAR(50)
AS
	SELECT DISTINCT Name FROM Tour WHERE HotelId IN ( SELECT Id FROM Hotel WHERE ResortId IN ( SELECT Id FROM Resort WHERE CountryId = (SELECT Id FROM Country WHERE Name = @countryName) ) )
								AND DepartureCityId = ( SELECT Id FROM DepartureCity WHERE Name = @departureCity )
