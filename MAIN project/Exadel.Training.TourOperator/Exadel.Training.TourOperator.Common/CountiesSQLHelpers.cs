﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Exadel.Training.TourOperator.Common
{
    public static class CountiesSQLHelpers
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TourOperatorDbContext"].ConnectionString;

        public static int CountryIdByName(string name)
        {
            int id = -1;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_country_id_by_name", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                // in parameter
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@name";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = name;
                sqlCmd.Parameters.Add(param);

                // out parameter
                param = new SqlParameter();
                param.ParameterName = "@id";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                id = ((int)sqlCmd.Parameters["@id"].Value);

                connection.Close();
            }

            return id;
        }
    }
}
