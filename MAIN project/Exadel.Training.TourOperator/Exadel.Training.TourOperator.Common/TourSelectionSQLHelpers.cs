﻿using Exadel.Training.TourOperator.Data.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Exadel.Training.TourOperator.Common
{
    public static class TourSelectionSQLHelpers
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TourOperatorDbContext"].ConnectionString;

        public static List<string> AllResortsOfACertainCountry(string countryName)
        {
            List<string> resorts = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_all_resorts_names_of_a_certain_country", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);

                        resorts.Add(name);
                    }
                }
            }

            return resorts;
        }

        public static List<string> AllTourTypesForCertainCountryAndDepartureCity(string countryName, string departureCity)
        {
            List<string> tours = new List<string>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_all_tours_names_for_certain_country_and_departure_city", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@departureCity";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = departureCity;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);

                        tours.Add(name);
                    }
                }
            }

            return tours;
        }

        public static List<Hotel> AllHotelsOfACertainCountry(string countryName)
        {
            List<Hotel> hotels = new List<Hotel>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_all_hotels_of_certain_country", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Hotel hotel = new Hotel();
                        hotel.Id = dr.GetInt32(0);
                        hotel.Name = dr.GetString(1);
                        hotel.Rating = dr.GetInt32(2);
                        hotel.ResortId = dr.GetInt32(3);
                        hotel.Location = dr.GetString(4);
                        hotel.Phone = dr.GetString(5);
                        try
                        {
                            hotel.WebSite = dr.GetString(6);
                        }
                        catch (SqlNullValueException)
                        {
                            hotel.WebSite = "";
                        }
                        try
                        {
                            hotel.Description = dr.GetString(7);
                        }
                        catch (SqlNullValueException)
                        {
                            hotel.Description = "";
                        }

                        hotels.Add(hotel);
                    }
                }
            }

            return hotels;
        }

        public static List<int> AllNightsAmountOfACertainCountry(string countryName)
        {
            List<int> nights = new List<int>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("Procedure_all_nights_amounts_for_certain_country", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@countryName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = countryName;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        int amount = dr.GetInt32(0);

                        nights.Add(amount);
                    }
                }
            }

            return nights;
        }
    }
}
