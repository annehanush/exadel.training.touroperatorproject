﻿using Exadel.Training.TourOperator.Data.Abstract;
using Exadel.Training.TourOperator.Data.Entities;
using Exadel.Training.TourOperator.WebUI.Areas.AccountArea.Models;
using Exadel.Training.TourOperator.Common;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Web.Mvc;
using Exadel.Training.TourOperator.WebUI.Models;

namespace Exadel.Training.TourOperator.WebUI.Controllers
{
    public class TourSelectionController : Controller
    {
        TourOperatorDbContext db = new TourOperatorDbContext();
        ApplicationContext context = new ApplicationContext();
        public ApplicationUser CurrentUser { get; private set; }

        private ICountryRepository countryRepository;
        private ITransportRepository transportRepository;
        private ITypeOfDietRepository tydRepository;
        private IDepartureCityRepository departureCityRepository;

        public TourSelectionController(ICountryRepository countryRepository, ITransportRepository transportRepository, ITypeOfDietRepository tydRepository, IDepartureCityRepository departureCityRepository)
        {
            this.countryRepository = countryRepository;
            this.transportRepository = transportRepository;
            this.tydRepository = tydRepository;
            this.departureCityRepository = departureCityRepository;
        }

        /// <summary>
        /// Check info about an existence of current user.
        /// </summary>
        /// <returns></returns>
        public string CheckUser()
        {
            string user = "";

            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            CurrentUser = UserManager.FindById(User.Identity.GetUserId());

            try
            {
                user = CurrentUser.UserName;
            }
            catch (NullReferenceException)
            {
                user = "";
            }

            return user;
        }

        /// <summary>
        /// Tour selection start view (if parameter is null, we use default values)
        /// </summary>
        /// <param name="currentCountryName"></param>
        /// <returns></returns>
        public ActionResult TourSelection(string currentCountryName)
        {
            ViewBag.User = CheckUser();

            if (currentCountryName != null)
            {
                ViewBag.CurrentCountry = currentCountryName;
            }
            else
            {
                ViewBag.CurrentCountry = "Turkey";
            }

            ViewBag.CurrentDepartureCity = "Kiev";

            var Countries = countryRepository.Countries;
            var CurrentResorts = TourSelectionSQLHelpers.AllResortsOfACertainCountry(ViewBag.CurrentCountry);
            var CurrentTourTypes = TourSelectionSQLHelpers.AllTourTypesForCertainCountryAndDepartureCity(ViewBag.CurrentCountry, ViewBag.CurrentDepartureCity);
            var CurrentHotels = TourSelectionSQLHelpers.AllHotelsOfACertainCountry(ViewBag.CurrentCountry);
            var Transports = transportRepository.Transports;
            var TYDs = tydRepository.TypeOfDiets;
            var DepartureCities = departureCityRepository.DepartureCities;
            var NightsAmounts = TourSelectionSQLHelpers.AllNightsAmountOfACertainCountry(ViewBag.CurrentCountry);

            MultipleModel model = new MultipleModel() { Countries = Countries, CurrentResorts = CurrentResorts, CurrentTourTypes = CurrentTourTypes, CurrentHotels = CurrentHotels, Transports = Transports, TypesOfDiet = TYDs, DepartureCities = DepartureCities, NightsAmounts = NightsAmounts };
            
            return View(model);
        }
    }
}