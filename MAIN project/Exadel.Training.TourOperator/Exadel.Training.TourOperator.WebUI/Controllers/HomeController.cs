﻿using Exadel.Training.TourOperator.Data.Abstract;
using Exadel.Training.TourOperator.Data.Entities;
using Exadel.Training.TourOperator.WebUI.Areas.AccountArea.Models;
using Exadel.Training.TourOperator.Common;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Web.Mvc;

namespace Exadel.Training.TourOperator.WebUI.Controllers
{
    public class HomeController : Controller
    {
        TourOperatorDbContext db = new TourOperatorDbContext();
        ApplicationContext context = new ApplicationContext();

        public ApplicationUser CurrentUser { get; private set; }

        private ICountryRepository countryRepository;
        private IFAQRepository faqRepository;

        public HomeController(ICountryRepository countryRepository, IFAQRepository faqRepository)
        {
            this.countryRepository = countryRepository;
            this.faqRepository = faqRepository;
        }

        /// <summary>
        /// Check info about an existence of current user.
        /// </summary>
        /// <returns></returns>
        public string CheckUser()
        {
            string user = "";

            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            CurrentUser = UserManager.FindById(User.Identity.GetUserId());

            try
            {
                user = CurrentUser.UserName;
            }
            catch (NullReferenceException)
            {
                user = "";
            }

            return user;
        }

        /// <summary>
        /// Index view.
        /// </summary>
        /// <returns></returns>
        public ViewResult Index()
        {
            ViewBag.User = CheckUser();
            return View();
        }

        /// <summary>
        /// Countries view. Gets info about all countries.
        /// </summary>
        /// <returns></returns>
        public ViewResult Countries(string name)
        {
            if (name == null)
            {
                ViewBag.CountryId = -1;
            }
            else
            {
                ViewBag.CountryId = CountiesSQLHelpers.CountryIdByName(name);
            }
            ViewBag.User = CheckUser();
            return View(countryRepository.Countries);
        }

        /// <summary>
        /// Info about choosed country. Returns partial view.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CertainCountryInfo(int id)
        {
            Country country = db.Countries.Find(id);

            return PartialView(country);
        }

        /// <summary>
        /// Contact view.
        /// </summary>
        /// <returns></returns>
        public ViewResult Contact()
        {
            ViewBag.User = CheckUser();
            return View();
        }

        /// <summary>
        /// FAQ view.
        /// </summary>
        /// <returns></returns>
        public ViewResult FAQ()
        {
            ViewBag.User = CheckUser();
            return View(faqRepository.FAQs);
        }

        /// <summary>
        /// Ask a quastion partial view.
        /// </summary>
        /// <returns></returns>
        public ActionResult AskAQuastion()
        {
            return PartialView();
        }

        /*
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AskAQuastion(QuastionModel model)
        {
            if (ModelState.IsValid)
            {
                // отправитель
                MailAddress from = new MailAddress("hannah.hanush97@gmail.com", "Hannah");
                // получатель
                MailAddress to = new MailAddress("anne.hanush97@gmail.com");

                MailMessage mail = new MailMessage();
                mail.From = from;
                mail.To.Add(to);

                var body = "<p>Message:</p><p>{0}</p>";

                mail.Subject = "New quastion in FAQ";
                mail.Body = string.Format(body, model.Message);
                mail.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                {
                    //smtp.Credentials = new NetworkCredential("hannah.hanush97@gmail.com", "26042604");
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    smtp.Send(mail);
                }

                return RedirectToAction("Sent");
            }
            return View();
        }
        */

        /// <summary>
        /// Sent view. We see this one after email was sent.
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        public ActionResult Sent(string question)
        {
            ViewBag.User = CheckUser();
            return View();
        }
    }
}