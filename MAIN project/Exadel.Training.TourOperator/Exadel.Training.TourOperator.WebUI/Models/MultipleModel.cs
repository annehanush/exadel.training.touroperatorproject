﻿using System.Collections.Generic;
using Exadel.Training.TourOperator.Data.Entities;

namespace Exadel.Training.TourOperator.WebUI.Models
{
    public class MultipleModel
    {
        public IEnumerable<Country> Countries { get; set; }
        public IEnumerable<string> CurrentResorts { get; set; }
        public IEnumerable<Hotel> CurrentHotels { get; set; }
        public IEnumerable<string> CurrentTourTypes { get; set; }
        public IEnumerable<Transport> Transports { get; set; }
        public IEnumerable<TypeOfDiet> TypesOfDiet { get; set; }
        public IEnumerable<DepartureCity> DepartureCities { get; set; }
        public IEnumerable<int> NightsAmounts { get; set; }
    }
}