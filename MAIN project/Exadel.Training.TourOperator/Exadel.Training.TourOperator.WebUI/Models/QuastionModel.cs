﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Exadel.Training.TourOperator.WebUI.Models
{
    public class QuastionModel
    {
        [Required(ErrorMessage = "Message filed should not be empty")]
        public string Message { get; set; }
    }
}