﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Exadel.Training.TourOperator.WebUI.Areas.AccountArea.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "UserName filed should not be empty")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password filed should not be empty")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}