﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace Exadel.Training.TourOperator.WebUI.Areas.AccountArea.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }

        public ApplicationUser() { }
    }
}