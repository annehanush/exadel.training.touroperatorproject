﻿using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;

namespace Exadel.Training.TourOperator.WebUI.Areas.AccountArea.Models
{
    public class CustomPasswordValidator : IIdentityValidator<string>
    {
        public int RequiredLength { get; set; } // минимальная длина

        public CustomPasswordValidator(int length)
        {
            RequiredLength = length;
        }

        public Task<IdentityResult> ValidateAsync(string item)
        {
            if (String.IsNullOrEmpty(item) || item.Length < RequiredLength)
            {
                return Task.FromResult(IdentityResult.Failed(
                                String.Format("Password length should be {0} characters at least", RequiredLength)));
            }

            return Task.FromResult(IdentityResult.Success);
        }
    }
}