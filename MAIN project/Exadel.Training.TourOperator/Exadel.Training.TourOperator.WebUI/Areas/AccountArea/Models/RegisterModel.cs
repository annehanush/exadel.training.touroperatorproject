﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Exadel.Training.TourOperator.WebUI.Areas.AccountArea.Models
{
    public class RegisterModel
    {
        [Required (ErrorMessage = "UserName filed should not be empty")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "EMail filed should not be empty")]
        public string Email { get; set; }

        public string FullName { get; set; }

        [Required (ErrorMessage = "Date of birth filed should not be empty")]
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }

        [Required(ErrorMessage = "Password filed should not be empty")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Password should be confirmed")]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}