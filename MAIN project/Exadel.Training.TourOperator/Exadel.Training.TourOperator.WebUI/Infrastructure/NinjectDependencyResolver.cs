﻿using Exadel.Training.TourOperator.Data.Abstract;
using Exadel.Training.TourOperator.Data.Concrete;
using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Exadel.Training.TourOperator.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            this.kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            kernel.Bind<ICountryRepository>().To<EFCountryRepository>();
            kernel.Bind<IFAQRepository>().To<EFFAQRepository>();
            kernel.Bind<ITransportRepository>().To<EFTransportRepository>();
            kernel.Bind<ITypeOfDietRepository>().To<EFTypeOfDietRepository>();
            kernel.Bind<IDepartureCityRepository>().To<EFDepartureCityRepository>();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
    }
}