﻿using Exadel.Training.TourOperator.Data.Abstract;
using Exadel.Training.TourOperator.Data.Entities;
using System.Collections.Generic;

namespace Exadel.Training.TourOperator.Data.Concrete
{
    public class EFTransportRepository : ITransportRepository
    {
        TourOperatorDbContext context = new TourOperatorDbContext();

        public IEnumerable<Transport> Transports
        {
            get
            {
                return context.Transports;
            }
        }
    }
}
