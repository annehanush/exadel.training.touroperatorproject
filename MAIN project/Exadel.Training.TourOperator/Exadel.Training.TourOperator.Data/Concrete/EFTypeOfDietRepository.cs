﻿using Exadel.Training.TourOperator.Data.Abstract;
using Exadel.Training.TourOperator.Data.Entities;
using System.Collections.Generic;
using System;

namespace Exadel.Training.TourOperator.Data.Concrete
{
    public class EFTypeOfDietRepository : ITypeOfDietRepository
    {
        TourOperatorDbContext context = new TourOperatorDbContext();

        public IEnumerable<TypeOfDiet> TypeOfDiets
        {
            get
            {
                return context.TypeOfDiets;
            }
        }
    }
}
