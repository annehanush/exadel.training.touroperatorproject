﻿using Exadel.Training.TourOperator.Data.Abstract;
using Exadel.Training.TourOperator.Data.Entities;
using System.Collections.Generic;

namespace Exadel.Training.TourOperator.Data.Concrete
{
    public class EFDepartureCityRepository : IDepartureCityRepository
    {
        TourOperatorDbContext context = new TourOperatorDbContext();

        public IEnumerable<DepartureCity> DepartureCities
        {
            get
            {
                return context.DepartureCities;
            }
        }
    }
}
