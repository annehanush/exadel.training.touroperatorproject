﻿using Exadel.Training.TourOperator.Data.Entities;
using System.Collections.Generic;

namespace Exadel.Training.TourOperator.Data.Abstract
{
    public interface ITypeOfDietRepository
    {
        IEnumerable<TypeOfDiet> TypeOfDiets { get; }
    }
}
