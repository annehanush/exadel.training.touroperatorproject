﻿using Exadel.Training.TourOperator.Data.Entities;
using System.Collections.Generic;

namespace Exadel.Training.TourOperator.Data.Abstract
{
    public interface ITransportRepository
    {
        IEnumerable<Transport> Transports { get; }
    }
}
