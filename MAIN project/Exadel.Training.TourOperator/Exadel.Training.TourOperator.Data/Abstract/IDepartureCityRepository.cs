﻿using Exadel.Training.TourOperator.Data.Entities;
using System.Collections.Generic;

namespace Exadel.Training.TourOperator.Data.Abstract
{
    public interface IDepartureCityRepository
    {
        IEnumerable<DepartureCity> DepartureCities { get; }
    }
}
