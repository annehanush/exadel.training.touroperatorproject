namespace Exadel.Training.TourOperator.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SetOfService
    {
        public int Id { get; set; }

        public int HotelId { get; set; }

        public int ServiceId { get; set; }

        public bool IsFree { get; set; }

        public virtual Hotel Hotel { get; set; }

        public virtual Service Service { get; set; }
    }
}
