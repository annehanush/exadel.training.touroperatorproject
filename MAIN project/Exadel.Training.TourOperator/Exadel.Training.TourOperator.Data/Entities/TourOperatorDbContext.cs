namespace Exadel.Training.TourOperator.Data.Entities
{
    using System.Data.Entity;

    public partial class TourOperatorDbContext : DbContext
    {
        public TourOperatorDbContext()
            : base("name=TourOperatorDbContext")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<C__RefactorLog> C__RefactorLog { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<DepartureCity> DepartureCities { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<FAQ> FAQs { get; set; }
        public virtual DbSet<FAQAnswer> FAQAnswers { get; set; }
        public virtual DbSet<Hotel> Hotels { get; set; }
        public virtual DbSet<HotelWithRoom> HotelWithRooms { get; set; }
        public virtual DbSet<HotelWithTYD> HotelWithTYDs { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
        public virtual DbSet<Resort> Resorts { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<SetOfService> SetOfServices { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Tour> Tours { get; set; }
        public virtual DbSet<Transport> Transports { get; set; }
        public virtual DbSet<TransportClass> TransportClasses { get; set; }
        public virtual DbSet<TransportType> TransportTypes { get; set; }
        public virtual DbSet<TypeOfDiet> TypeOfDiets { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Client>()
                .Property(e => e.Gender)
                .IsFixedLength();

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Requests)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.VisaCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Resorts)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DepartureCity>()
                .HasMany(e => e.Tours)
                .WithRequired(e => e.DepartureCity)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FAQ>()
                .HasMany(e => e.FAQAnswers)
                .WithRequired(e => e.FAQ)
                .HasForeignKey(e => e.QuastionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.HotelWithRooms)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.HotelWithTYDs)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.SetOfServices)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hotel>()
                .HasMany(e => e.Tours)
                .WithRequired(e => e.Hotel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HotelWithRoom>()
                .HasMany(e => e.Tours)
                .WithRequired(e => e.HotelWithRoom)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HotelWithTYD>()
                .HasMany(e => e.Tours)
                .WithRequired(e => e.HotelWithTYD)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Resort>()
                .HasMany(e => e.Hotels)
                .WithRequired(e => e.Resort)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Room>()
                .HasMany(e => e.HotelWithRooms)
                .WithRequired(e => e.Room)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.SetOfServices)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tour>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Tour>()
                .HasMany(e => e.Requests)
                .WithRequired(e => e.Tour)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Transport>()
                .HasMany(e => e.Tours)
                .WithRequired(e => e.Transport)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TransportClass>()
                .HasMany(e => e.Transports)
                .WithRequired(e => e.TransportClass)
                .HasForeignKey(e => e.ClassId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TransportType>()
                .HasMany(e => e.Transports)
                .WithRequired(e => e.TransportType)
                .HasForeignKey(e => e.TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypeOfDiet>()
                .HasMany(e => e.HotelWithTYDs)
                .WithRequired(e => e.TypeOfDiet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Clients)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
