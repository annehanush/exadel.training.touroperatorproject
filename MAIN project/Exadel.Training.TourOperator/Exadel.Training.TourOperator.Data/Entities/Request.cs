namespace Exadel.Training.TourOperator.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Request")]
    public partial class Request
    {
        public int Id { get; set; }

        public bool Insurance { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateOfCreation { get; set; }

        public int TourId { get; set; }

        public int ClientId { get; set; }

        public virtual Client Client { get; set; }

        public virtual Tour Tour { get; set; }
    }
}
