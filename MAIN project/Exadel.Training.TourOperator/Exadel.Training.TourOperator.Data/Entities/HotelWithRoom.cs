namespace Exadel.Training.TourOperator.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HotelWithRoom")]
    public partial class HotelWithRoom
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HotelWithRoom()
        {
            Tours = new HashSet<Tour>();
        }

        public int Id { get; set; }

        public int HotelId { get; set; }

        public int RoomId { get; set; }

        public int? AmountOfRooms { get; set; }

        [StringLength(50)]
        public string Square { get; set; }

        public int? PeopleAmount { get; set; }

        public string Info { get; set; }

        public virtual Hotel Hotel { get; set; }

        public virtual Room Room { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tour> Tours { get; set; }
    }
}
