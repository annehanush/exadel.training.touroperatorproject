namespace Exadel.Training.TourOperator.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FAQAnswer")]
    public partial class FAQAnswer
    {
        public int Id { get; set; }

        [Required]
        public string Answer { get; set; }

        public int QuastionId { get; set; }

        public virtual FAQ FAQ { get; set; }
    }
}
